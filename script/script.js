const url = "https://jsonplaceholder.typicode.com/comments";
const pageComments = 10;

function createPaginationBtn(pageNumber) {
  const div = document.createElement("div");
  div.classList.add('pagination', 'js_pagination');
  div.dataset.page = pageNumber;
  div.innerHTML = pageNumber;
  return div;
}

function addPaginationListener() {
  document.querySelectorAll('.js_pagination').forEach((pagination) => {
    pagination.addEventListener("click", (e) => {
      let container = document.querySelector('.card');
      container.innerHTML = '';

      let pageNumber = e.target.dataset.page;
      //кнопка вызова таблици
      let reqURL = url + getParams(pageNumber);
      const req = new XMLHttpRequest();
      req.open("GET", reqURL); ///вызов ссылки
      req.send();
      req.addEventListener("readystatechange", () => {
        if (req.readyState === 4) {
          if (req.status >= 200 && req.status < 300) {
            showCardMassage(JSON.parse(req.responseText));
          } else {
            throw new Error("Помилка у запиті");
          }
        }
      });
    });
  });
}

function countPages() {
  const reqURL = url;
  const req = new XMLHttpRequest();
  req.open("GET", reqURL); ///вызов ссылки
  req.send();
  req.addEventListener("readystatechange", () => {
    if (req.readyState === 4) {
      if (req.status >= 200 && req.status < 300) {
        const container = document.querySelector('.paginations');
        let commentsCount = JSON.parse(req.responseText).length;
        let pages = Math.ceil(commentsCount / pageComments) - 1;

        for (let i = 0; i < pages; i++) {
          container.append(createPaginationBtn(i + 1));
        }

        addPaginationListener();
      } else {
        throw new Error("Помилка у запиті");
      }
    }
  });
}

function firstLoad() {
  let pageNumber = 0;
  //кнопка вызова таблици
  const reqURL = url + getParams(pageNumber);
  const req = new XMLHttpRequest();
  req.open("GET", reqURL); ///вызов ссылки
  req.send();
  req.addEventListener("readystatechange", () => {
    if (req.readyState === 4) {
      if (req.status >= 200 && req.status < 300) {
        showCardMassage(JSON.parse(req.responseText));
      } else {
        throw new Error("Помилка у запиті");
      }
    }
  });
}

window.addEventListener("load", () => {
  let container = document.querySelector('.paginations');
  if (container.innerHTML.length) {
    return;
  }

  countPages();
  firstLoad();
});

function getParams(pageNumber) {
  let result = '?';
  for (let i = 1; i <= pageComments; i++) {
    result += 'id=' + (i + (pageComments * pageNumber));

    if (i !== 10) {
      result += '&'
    }
  }

  return result;
}

function showCardMassage(data = []) {
  if (!Array.isArray(data)) {
    throw new Error("З сервера прийшов не масив!");
  }

  let container = document.querySelector('.card');
  container.innerHTML = '';

  data.forEach((obj) => {
    container.append(createMasage(obj))
  });
}
function createMasage({id, name, email, body, ...props}) {
  const div = document.createElement("div");
  div.classList.add('comment');
  div.insertAdjacentHTML("beforeend", `<div># ${id}</div>
  <div class="info-user">
  <div class="name">Ім'я: ${name}</div>
   <div class="email">Електрона пошта: ${email}</div>
   </div>
   <div class="message-body">Повідомлення: ${body}</div>`);
   return div;
}

